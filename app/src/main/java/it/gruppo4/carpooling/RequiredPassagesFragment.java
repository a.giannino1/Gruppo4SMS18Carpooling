package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import Custom.CustomAdapterRequired;
import models.PassaggioOfferto;
import models.User;

public class RequiredPassagesFragment extends ListFragment {

    private String userID = LoggedUserSession.Instance().getUID();
    private String aziendaID = LoggedUserSession.Instance().getAziendaID();

    private DatabaseReference mRef;
    private ArrayList<PassaggioOfferto> arrayPassaggiRichiesti = new ArrayList<>();
    private ArrayList<String> arrayConducenti = new ArrayList<>();
    private ArrayList<String> arrayidPassaggiRichiesti = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRef = FirebaseDatabase.getInstance().getReference().child("Azienda").child(aziendaID).child("dipendenti");

        getPassaggiRichiesti();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getPassaggiRichiesti() {
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    if(!dipendente.getKey().equals(userID)) {
                        for (DataSnapshot passaggio : dipendente.child("passaggiOfferti").getChildren()) {
                            for (DataSnapshot passeggero : passaggio.child("passeggeri").getChildren()) {
                                if(passeggero.getKey().equals(userID)) {
                                    arrayPassaggiRichiesti.add(passaggio.getValue(PassaggioOfferto.class));
                                    arrayidPassaggiRichiesti.add(passaggio.getKey());
                                    arrayConducenti.add(dipendente.getKey());
                                }
                            }
                        }
                    }
                }

                if (getContext() != null) {
                    CustomAdapterRequired customAdapter = new CustomAdapterRequired(getContext(), arrayPassaggiRichiesti);
                    setListAdapter(customAdapter);
                    registerForContextMenu(getListView());
                }

                if(arrayPassaggiRichiesti.isEmpty()) {
                    Toast.makeText(getContext(), getResources().getString(R.string.noPassage), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                RequiredMapDialogFragment newFragment = new RequiredMapDialogFragment();
                Bundle b = new Bundle();

                if (!arrayPassaggiRichiesti.isEmpty()){
                    b.putString("idRichiesto", arrayidPassaggiRichiesti.get(pos));
                    b.putString("conducente", arrayConducenti.get(pos));
                    newFragment.setArguments(b);
                    newFragment.show(getFragmentManager(), "mapDialog");
                }
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, v.getId(), 0, R.string.callAutista);
        menu.add(0, v.getId(), 0, R.string.deleteRe);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = null;
        if(item.getMenuInfo() != null && item.getMenuInfo() instanceof AdapterView.AdapterContextMenuInfo){
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        }
        final int position = info.position;

        if(item.getTitle().equals(getResources().getString(R.string.callAutista))){
            callAutista(position);
        }
        else if (item.getTitle().equals(getResources().getString(R.string.deleteRe))){

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.confirm);
            builder.setMessage(R.string.areyousure);

            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    deletePassageRe(position);
                }
            });

            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
        else {
            return false;
        }
        return true;
    }

    private void callAutista(final int position) {
        mRef.addValueEventListener(new ValueEventListener() {
            int i = -1;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    if(!dipendente.getKey().equals(userID)) {
                        for (DataSnapshot passaggio : dipendente.child("passaggiOfferti").getChildren()) {
                            for (DataSnapshot passeggero : passaggio.child("passeggeri").getChildren()) {
                                if(passeggero.getKey().equals(userID)) {
                                    i++;
                                    if (i == position){
                                        User d = dipendente.getValue(User.class);
                                        String phone = d.getCellulare() + "";
                                        Intent intentCall = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                                        startActivity(intentCall);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void deletePassageRe(final int position) {
        mRef.addValueEventListener(new ValueEventListener() {
            int i = -1;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    if(!dipendente.getKey().equals(userID)) {
                        for (DataSnapshot passaggio : dipendente.child("passaggiOfferti").getChildren()) {
                            for (DataSnapshot passeggero : passaggio.child("passeggeri").getChildren()) {
                                if(passeggero.getKey().equals(userID)) {
                                    i++;
                                    if (i == position){
                                        mRef.child(dipendente.getKey()).child("passaggiOfferti").child(passaggio.getKey()).
                                                child("passeggeri").child(userID).removeValue();

                                        Toast.makeText(getActivity(), getResources().getString(R.string.eliminato), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
