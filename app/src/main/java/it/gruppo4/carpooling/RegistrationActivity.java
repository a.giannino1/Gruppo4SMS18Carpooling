package it.gruppo4.carpooling;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import models.Azienda;
import models.User;

public class RegistrationActivity extends AppCompatActivity {

    private EditText inputEmail, inputPassword, inputNome, inputCognome, inputCitta, inputVia, inputCellulare;
    private Button btnSignIn, btnSignUp;
    private ProgressBar progressBar;
    private Spinner spinnerAz;
    String azienda;
    ArrayList<String> listAz = new ArrayList<String>();
    ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.car);
        //Get Firebase auth instance
        btnSignIn = (Button) findViewById(R.id.sign_in_button);
        btnSignUp = (Button) findViewById(R.id.sign_up_button);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        inputNome = (EditText) findViewById(R.id.txtnome);
        inputCognome = (EditText) findViewById(R.id.txtcognome);
        inputCitta = (EditText) findViewById(R.id.txtcitta);
        inputVia = (EditText) findViewById(R.id.txtvia);
        inputCellulare = (EditText) findViewById(R.id.txtcellulare);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        spinnerAz = (Spinner) findViewById(R.id.spinnerAz);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listAz);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Azienda");;

        //Leggere dal Database tutte le aziende
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                    Azienda u = azienda.getValue(Azienda.class);
                    System.out.println(u.toString());

                    //come popolare uno spinner
                    listAz.add(u.getNome());
                    adapter.notifyDataSetChanged();
                    spinnerAz.setAdapter(adapter);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                String nome = inputNome.getText().toString().trim();
                String cognome = inputCognome.getText().toString().trim();
                String citta = inputCitta.getText().toString().trim();
                String via = inputVia.getText().toString().trim();
                String phone = inputCellulare.getText().toString().trim();
                final String stato = "in sospeso";
                final int punti = 0;

                if (TextUtils.isEmpty(nome)) {
                    Toast.makeText(getApplicationContext(), "Enter name ", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(cognome)) {
                    Toast.makeText(getApplicationContext(), "Enter surname!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(citta)) {
                    Toast.makeText(getApplicationContext(), "Enter city!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(via)) {
                    Toast.makeText(getApplicationContext(), "Enter address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(getApplicationContext(), "Enter phone!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (phone.length() < 10) {
                    Toast.makeText(getApplicationContext(), "unvalid phone!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }


                progressBar.setVisibility(View.VISIBLE);
                //create user
                final FirebaseAuth mFirebaseAuth;
                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                //Toast.makeText(RegistrationActivity.this, "Loading " + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                Toast.makeText(RegistrationActivity.this, "Loading", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(RegistrationActivity.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {

                                    final String mUserId;
                                    final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");
                                    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();


                                    mUserId = mFirebaseUser.getUid();
                                    azienda = spinnerAz.getSelectedItem().toString();
                                    mDatabase.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            for (DataSnapshot aziendaD : dataSnapshot.getChildren()) {
                                                Azienda a = aziendaD.getValue(Azienda.class);

                                                if(a.getNome().equals(azienda)){
                                                    User user;
                                                    user = new User();
                                                    long cellulare = new Long(inputCellulare.getText().toString().trim());
                                                    user.setNome(inputNome.getText().toString().trim());
                                                    user.setCognome(inputCognome.getText().toString().trim());
                                                    user.setCitta(inputCitta.getText().toString().trim());
                                                    user.setVia(inputVia.getText().toString().trim());
                                                    user.setCellulare(cellulare);
                                                    user.setMail(inputEmail.getText().toString().trim());
                                                    user.setPassword(inputPassword.getText().toString().trim());
                                                    user.setStato(stato);
                                                    user.setPunti(punti);

                                                    mDatabase.child(aziendaD.getKey()).child("dipendenti").child(mUserId).setValue(user);

                                                    startActivity(new Intent(RegistrationActivity.this, HomeActivity.class));
                                                    finish();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                        });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}
