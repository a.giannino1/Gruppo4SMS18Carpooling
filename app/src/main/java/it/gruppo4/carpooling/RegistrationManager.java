package it.gruppo4.carpooling;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import models.Azienda;
import models.User;

public class RegistrationManager extends AppCompatActivity {

    private EditText inputEmail, inputPassword, inputNome, inputCognome, inputCitta, inputVia, inputCellulare;
    private Button btnSignIn, btnSignUp;
    private ProgressBar progressBar;
    private String infoPIAzienda;
    private String infoNomeAzienda;
    private String infoCittaAzienda;
    private String infoViaAzienda;
    private FirebaseAuth mFirebaseAuth;
    private String mUserId;

    private String email;
    private String password;
    private String nome;
    private String cognome;
    private String citta;
    private String via;
    private String phone;
    private String stato;
    private int punti;

    private long cellulare;

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");

    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_manager);
        //Get Firebase auth instance
        btnSignIn = (Button) findViewById(R.id.sign_in_button);
        btnSignUp = (Button) findViewById(R.id.btnsign_up_manager);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        inputNome = (EditText) findViewById(R.id.txtnome);
        inputCognome = (EditText) findViewById(R.id.txtcognome);
        inputCitta = (EditText) findViewById(R.id.txtcitta);
        inputVia = (EditText) findViewById(R.id.txtvia);
        inputCellulare = (EditText) findViewById(R.id.txtcellulare);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        Intent intent = getIntent();
        infoPIAzienda = intent.getStringExtra("infoPIAzienda");
        infoNomeAzienda = intent.getStringExtra("infoNomeAzienda");
        infoCittaAzienda = intent.getStringExtra("infoCittaAzienda");
        infoViaAzienda = intent.getStringExtra("infoViaAzienda");

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();
                nome = inputNome.getText().toString().trim();
                cognome = inputCognome.getText().toString().trim();
                citta = inputCitta.getText().toString().trim();
                via = inputVia.getText().toString().trim();
                phone = inputCellulare.getText().toString().trim();
                stato = "Confermato";
                punti = 0;

                if (TextUtils.isEmpty(nome)) {
                    Toast.makeText(getApplicationContext(), "Enter name ", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(cognome)) {
                    Toast.makeText(getApplicationContext(), "Enter surname!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(citta)) {
                    Toast.makeText(getApplicationContext(), "Enter city!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(via)) {
                    Toast.makeText(getApplicationContext(), "Enter address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(getApplicationContext(), "Enter phone!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (phone.length() < 10) {
                    Toast.makeText(getApplicationContext(), "unvalid phone!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                //create user
                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegistrationManager.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                //Toast.makeText(RegistrationActivity.this, "Loading " + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                Toast.makeText(RegistrationManager.this, "Loading", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(RegistrationManager.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {

                                    mFirebaseUser = mFirebaseAuth.getCurrentUser();

                                    mUserId = mFirebaseUser.getUid();


                                            User user = new User();
                                            cellulare = Long.valueOf(inputCellulare.getText().toString().trim());
                                            user.setNome(nome);
                                            user.setCognome(cognome);
                                            user.setCitta(citta);
                                            user.setVia(via);
                                            user.setCellulare(cellulare);
                                            user.setMail(email);
                                            user.setPassword(password);
                                            user.setStato(stato);
                                            user.setPunti(punti);

                                            Azienda azienda = new Azienda();
                                            azienda.setNome(infoNomeAzienda);
                                            azienda.setCitta(infoCittaAzienda);
                                            azienda.setVia(infoViaAzienda);
                                            azienda.setManager(mUserId);

                                            mDatabase.child(infoPIAzienda).setValue(azienda);
                                            mDatabase.child(infoPIAzienda).child("dipendenti").child(mUserId).setValue(user);

                                            startActivity(new Intent(RegistrationManager.this, HomeActivity.class));
                                            finish();

                                }
                            }
                        });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}