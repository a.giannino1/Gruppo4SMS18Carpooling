package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import models.Azienda;
import models.Macchina;
import models.User;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private DrawerLayout drawer;
    NavigationView navigationView;
    BottomNavigationView bottomNavigationView;

    public String currentUID;
    public String currentAziendaUID;

    //NAV HEADER
    TextView nav_user_name;
    TextView nav_user_city;

    private String CONFERMATO = "Confermato";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            loadLogInView();
        }

        currentUID = FirebaseAuth.getInstance().getUid();

        if (savedInstanceState == null) {
            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                        for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                            if (dipendente.getKey().equals(currentUID)) {
                                Azienda a = azienda.getValue(Azienda.class);
                                User u = dipendente.getValue(User.class);
                                LoggedUserSession.Instance().setUser(u);
                                LoggedUserSession.Instance().setAzienda(a);
                                LoggedUserSession.Instance().setAziendaID(azienda.getKey());
                                LoggedUserSession.Instance().setUID(currentUID);
                                currentUID = LoggedUserSession.Instance().getUID();
                                currentAziendaUID = LoggedUserSession.Instance().getAziendaID();
                                if (LoggedUserSession.Instance().getAzienda().getManager().equals(currentUID)) {
                                    navigationView.inflateMenu(R.menu.activity_home_drawer_manager);
                                } else {
                                    navigationView.inflateMenu(R.menu.activity_home_drawer);
                                }
                                getUserInfo();
                            }
                        }
                    }
                }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.flContent, new HomeFragment());
        tx.commit();

        setupDrawerContent(navigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // handle desired action here
                switch (item.getItemId()) {
                    case R.id.nav_serach_passage:
                        if(LoggedUserSession.Instance().getUser().getStato().equals(CONFERMATO)){
                            Intent goCercaPassaggio = new Intent(HomeActivity.this, CercaPassaggio.class);
                            startActivity(goCercaPassaggio);
                        }
                        else{
                            Toast.makeText(HomeActivity.this, "Account non ancora confermato ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.nav_give_passage:
                        if(LoggedUserSession.Instance().getUser().getStato().equals(CONFERMATO)){
                            Intent goCercaPassaggio = new Intent(HomeActivity.this, OffriPassaggio.class);
                            startActivity(goCercaPassaggio);
                        }
                        else{
                            Toast.makeText(HomeActivity.this, "Account non ancora confermato ", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
                return false;
            }
        });
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass;
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.nav_home:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.nav_passages:
                fragmentClass = MyPassages.class;
                break;
            case R.id.nav_today_passages:
                fragmentClass = TodayPassageActivity.class;
                break;
            case R.id.nav_classifica:
                fragmentClass = Classifica.class;
                break;
            case R.id.nav_manage_dip:
                fragmentClass = GestioneDipendenti.class;
                break;
            case R.id.nav_manage_fact:
                fragmentClass = GestioneAziendale.class;
                break;
            case R.id.nav_manage_turni:
                fragmentClass = GestioneTurni.class;
                break;
            default:
                fragmentClass = HomeFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        drawer.closeDrawer(GravityCompat.START);

    }


    private void getUserInfo() {
        View mView = navigationView.getHeaderView(0);
        nav_user_name = (TextView) mView.findViewById(R.id.nome);
        nav_user_city = (TextView) mView.findViewById(R.id.citta);

        nav_user_name.setText(LoggedUserSession.Instance().getUser().getNome() + " " + LoggedUserSession.Instance().getUser().getCognome());
        nav_user_city.setText(LoggedUserSession.Instance().getUser().getCitta());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        selectDrawerItem(item);

        return true;
    }

    private void loadLogInView() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tool_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                //logging out the user
                mFirebaseAuth.signOut();
                //closing activity
                finish();
                //starting login activity
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.action_add_car:
                aggiungiMacchina();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Funzione per aggiungere una macchina
    public void aggiungiMacchina() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HomeActivity.this);
        LayoutInflater inflater = HomeActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.add_car_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Aggiungo un auto");
        final EditText editTextModello = (EditText) dialogView.findViewById(R.id.editTextModello);
        final EditText editTextPosti = (EditText) dialogView.findViewById(R.id.editTextPostin);
        dialogBuilder.setPositiveButton("Aggiungo la macchina", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot a : dataSnapshot.getChildren()) {
                            for (DataSnapshot d : a.child("dipendenti").getChildren()) {
                                if (d.getKey().equals(currentUID)) {
                                    //inserire macchina
                                    Macchina m = new Macchina();
                                    m.setModello(editTextModello.getText().toString());
                                    m.setPosti(Long.parseLong(editTextPosti.getText().toString()));
                                    mDatabase.child(a.getKey()).child("dipendenti").child(d.getKey()).child("macchine").child("macchinaNuova" + m.getModello()).setValue(m);
                                    Toast.makeText(getApplicationContext(), "Macchina aggiunta!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }
}