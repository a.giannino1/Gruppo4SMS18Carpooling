package it.gruppo4.carpooling;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class RegistrationAzienda extends AppCompatActivity {

    private EditText inputNomeAzienda, inputCittaAzienda, inputViaAzienda, inputPI;
    private Button btnAvanti;
    private ProgressBar progressBar;

    private String nomeAzienda;
    private String PIAzienda;
    private String cittaAzienda;
    private String viaAzienda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_azienda);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.car);
        //Get Firebase auth instance
        btnAvanti = (Button) findViewById(R.id.btn_avanti);
        inputNomeAzienda = (EditText) findViewById(R.id.txtnome_azienda);
        inputCittaAzienda = (EditText) findViewById(R.id.txtcittaAzienda);
        inputViaAzienda = (EditText) findViewById(R.id.txtviaAzienda);
        inputPI = (EditText) findViewById(R.id.txtPI);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnAvanti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nomeAzienda = inputNomeAzienda.getText().toString().trim();
                cittaAzienda = inputCittaAzienda.getText().toString().trim();
                viaAzienda = inputViaAzienda.getText().toString().trim();
                PIAzienda = inputPI.getText().toString().trim();

                if (TextUtils.isEmpty(nomeAzienda)) {
                    Toast.makeText(getApplicationContext(), "Enter name azienda ", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(cittaAzienda)) {
                    Toast.makeText(getApplicationContext(), "Enter city azienda!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(viaAzienda)) {
                    Toast.makeText(getApplicationContext(), "Enter address azienda!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(PIAzienda)) {
                    Toast.makeText(getApplicationContext(), "Enter partita iva!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (PIAzienda.length() != 11) {
                    Toast.makeText(getApplicationContext(), "unvalid partita iva!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                Intent goManager = new Intent(RegistrationAzienda.this, RegistrationManager.class);
                goManager.putExtra("infoPIAzienda", PIAzienda);
                goManager.putExtra("infoNomeAzienda", nomeAzienda);
                goManager.putExtra("infoCittaAzienda", cittaAzienda);
                goManager.putExtra("infoViaAzienda", viaAzienda);

                startActivity(goManager);
                finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}