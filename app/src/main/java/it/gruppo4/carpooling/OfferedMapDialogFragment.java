package it.gruppo4.carpooling;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import models.PassaggioOfferto;
import models.Passeggero;
import models.User;

public class OfferedMapDialogFragment extends DialogFragment implements OnMapReadyCallback{

    private String userID = LoggedUserSession.Instance().getUID();
    private String aziendaID = LoggedUserSession.Instance().getAziendaID();
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Azienda").child(aziendaID).child("dipendenti");
    private Double lat, lon;
    private ArrayList<String> idPasseggeri = new ArrayList<>();
    private ArrayList<User> passeggeri = new ArrayList<>();
    private ArrayList<Double> latitudine = new ArrayList<>();
    private ArrayList<Double> longitudine = new ArrayList<>();
    private String idPassaggio;
    private TextView nomeO, phoneNumber, postiO;
    private User userMarker;
    private String userMarkerId;
    private int i;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        idPassaggio = getArguments() != null ? getArguments().getString("idPassaggio") : null;

        getViePasseggeriFromPassaggio(idPassaggio);

        View rootView = inflater.inflate(R.layout.fragment_map_dialog, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(OfferedMapDialogFragment.this);

        return rootView;
    }

    private void getViePasseggeriFromPassaggio(final String idPassaggio) {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dipendente : dataSnapshot.getChildren()){
                    for(DataSnapshot passaggio : dipendente.child("passaggiOfferti").getChildren()){
                        if(idPassaggio.equals(passaggio.getKey())){
                            PassaggioOfferto p = passaggio.getValue(PassaggioOfferto.class);
                            for (Map.Entry<String, Passeggero> passeggero : p.getPasseggeri().entrySet()) {
                                idPasseggeri.add(passeggero.getKey());
                            }
                        }
                    }
                }

                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    for (i = 0; i < idPasseggeri.size(); i++){
                        if(dipendente.getKey().equals(idPasseggeri.get(i))){
                            User u = dipendente.getValue(User.class);
                            passeggeri.add(u);
                            converter(u.getVia());
                            latitudine.add(lat);
                            longitudine.add(lon);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public OfferedMapDialogFragment() {}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        int lenght = latitudine.size();
        for(i = 0; i < lenght; i++) {
            LatLng latLng = new LatLng(latitudine.get(i), longitudine.get(i));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            String idPasse = idPasseggeri.get(i);

            if (idPasse.equals(userID)){
                markerOptions.title(getResources().getString(R.string.casaMia));
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }else{
                markerOptions.title(getResources().getString(R.string.passeggero));
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }

            googleMap.addMarker(markerOptions);

            if (!idPasse.equals(userID)) {
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public boolean onMarkerClick(final Marker marker) {

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.offered_dialog, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setTitle(getResources().getString(R.string.infoPasseggero));

                        nomeO = (TextView) dialogView.findViewById(R.id.nomeO);
                        phoneNumber = (TextView) dialogView.findViewById(R.id.phoneNumber);

                        LatLng pos = marker.getPosition();
                        for (int i = 0; i < passeggeri.size(); i++) {
                            if(latitudine.get(i).equals(pos.latitude)) {
                                userMarker = passeggeri.get(i);
                                getIdFromUser(userMarker);
                                nomeO.setText(userMarker.getNome());
                                phoneNumber.setText(userMarker.getCellulare() + "");

                                postiO = (TextView) dialogView.findViewById(R.id.postiO);
                                mDatabase.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                                                    for (int i = 0; i < idPasseggeri.size(); i++) {
                                                        if(dipendente.getKey().equals(idPasseggeri.get(i))) {
                                                            for (DataSnapshot passaggi : dipendente.child("passaggiOfferti").getChildren()) {
                                                                if(passaggi.getKey().equals(idPassaggio)) {
                                                                    PassaggioOfferto passOffCorrente = passaggi.getValue(PassaggioOfferto.class);
                                                                    postiO.setText(getResources().getString(R.string.postiDiponibili) + passOffCorrente.getPostiDisponibili());
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });

                                dialogBuilder.setPositiveButton(getResources().getString(R.string.accetta), new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, int whichButton) {
                                        mDatabase.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                                                    for (int i = 0; i < idPasseggeri.size(); i++) {
                                                        if(dipendente.getKey().equals(idPasseggeri.get(i))) {
                                                            System.out.println("DIPENDENTE CHE OFFRE IL PASSAGGIO " + dipendente.getKey() + " == " + idPasseggeri.get(i));
                                                            for (DataSnapshot passaggi : dipendente.child("passaggiOfferti").getChildren()) {
                                                                if(passaggi.getKey().equals(idPassaggio)) {
                                                                    System.out.println(passaggi.getKey() + " == " + idPassaggio);
                                                                    PassaggioOfferto pass = passaggi.getValue(PassaggioOfferto.class);
                                                                    for (DataSnapshot passeggeriN : passaggi.child("passeggeri").getChildren()) {
                                                                        System.out.println("passeggeriN " + passeggeriN.getKey());
                                                                        Passeggero u = passeggeriN.getValue(Passeggero.class);
                                                                        System.out.println("userMarkerId " + userMarkerId);
                                                                        if(passeggeriN.getKey().equals(userMarkerId)) {
                                                                            System.out.println(passeggeriN.getKey() + " == " + userMarkerId);
                                                                            Passeggero p = new Passeggero();
                                                                            p.setStato_richiesta_passaggio("Accettato");
                                                                            p.setTracking("Fermo");
                                                                            mDatabase.child(dipendente.getKey()).child("passaggiOfferti").child(idPassaggio).
                                                                                    child("passeggeri").child(passeggeriN.getKey()).setValue(p);
                                                                            }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    }
                                });
                                dialogBuilder.setNegativeButton(getResources().getString(R.string.rifiuta), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        mDatabase.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                                                    for (int i = 0; i < idPasseggeri.size(); i++) {
                                                        if(dipendente.getKey().equals(idPasseggeri.get(i))) {
                                                            System.out.println("DIPENDENTE CHE OFFRE IL PASSAGGIO " + dipendente.getKey() + " == " + idPasseggeri.get(i));
                                                            for (DataSnapshot passaggi : dipendente.child("passaggiOfferti").getChildren()) {
                                                                if(passaggi.getKey().equals(idPassaggio)) {
                                                                    System.out.println(passaggi.getKey() + " == " + idPassaggio);
                                                                    for (DataSnapshot passeggeriN : passaggi.child("passeggeri").getChildren()) {
                                                                        System.out.println("passeggeriN " + passeggeriN.getKey());
                                                                        Passeggero u = passeggeriN.getValue(Passeggero.class);
                                                                        System.out.println("userMarkerId " + userMarkerId);
                                                                        if(passeggeriN.getKey().equals(userMarkerId)) {
                                                                            System.out.println(passeggeriN.getKey() + " == " + userMarkerId);
                                                                            Passeggero p = new Passeggero();
                                                                            p.setStato_richiesta_passaggio("Rifiutato");
                                                                            p.setTracking("Fermo");
                                                                            mDatabase.child(dipendente.getKey()).child("passaggiOfferti").child(idPassaggio).
                                                                                    child("passeggeri").child(passeggeriN.getKey()).setValue(p);

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    }
                                });
                            }
                        }

                        dialogBuilder.create().show();
                        return false;
                    }
                });
            }
        }
    }

    public void converter(String string) {
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocationName(string, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            lat = addresses.get(0).getLatitude();
            lon = addresses.get(0).getLongitude();
            System.out.println(lat);
            System.out.println(lon);
        }
    }

    private String getIdFromUser(final User user){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    User u = dipendente.getValue(User.class);

                    if(u.getCellulare() == (user.getCellulare())){
                        userMarkerId = dipendente.getKey();
                      }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return userMarkerId;
    }

    public void onDestroyView(){
        super.onDestroyView();
        Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }
}
