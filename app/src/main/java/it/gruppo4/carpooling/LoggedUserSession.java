package it.gruppo4.carpooling;

import models.User;
import models.Azienda;

public class LoggedUserSession {
    private static LoggedUserSession instance;
    private String uid;
    private String aziendaID;
    User user;
    Azienda azienda;
    //no outer class can initialize this class's object
    private LoggedUserSession() {}

    public static LoggedUserSession Instance()
    {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null)
        {
            instance = new LoggedUserSession();
        }
        return instance;
    }

    public String getUID() {
        return uid;
    }

    public void setUID(String uid) {
        this.uid = uid;
    }
    public String getAziendaID() {
        return aziendaID;
    }

    public void setAziendaID(String aziendaID) {
        this.aziendaID = aziendaID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Azienda getAzienda() {
        return azienda;
    }

    public void setAzienda(Azienda azienda) {
        this.azienda = azienda;
    }
}
