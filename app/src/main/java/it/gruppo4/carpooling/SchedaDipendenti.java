package it.gruppo4.carpooling;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import Custom.CustomAdapterDettaglioDipendenti;
import models.PassaggioOfferto;
import models.User;

public class SchedaDipendenti extends AppCompatActivity {

    private String aziendaID = LoggedUserSession.Instance().getAziendaID();

    private DatabaseReference mRef = FirebaseDatabase.getInstance().getReference("Azienda").child(aziendaID).child("dipendenti");
    String message;

    ArrayList<PassaggioOfferto> listPassaggi = new ArrayList<PassaggioOfferto>();
    ListView mylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheda_dipendenti);

        mylist = (ListView) findViewById(R.id.listaDipendenti);
        Intent intent = getIntent();
        message = intent.getStringExtra("infoDipendente");

        getAllPassaggi();

    }

    private void getAllPassaggi(){
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String current = "";
                for(DataSnapshot dipendente : dataSnapshot.getChildren()){
                    User u = dipendente.getValue(User.class);
                    if (u.getNome().equals(message)){
                        current = dipendente.getKey();
                    }
                }

                for(DataSnapshot dipendente : dataSnapshot.getChildren()){
                    for (DataSnapshot passaggio : dipendente.child("passaggiOfferti").getChildren()) {
                        for (DataSnapshot passeggero : passaggio.child("passeggeri").getChildren()) {
                            if(passeggero.getKey().equals(current)) {
                                PassaggioOfferto pass = passaggio.getValue(PassaggioOfferto.class);
                                listPassaggi.add(pass);
                            }
                        }
                    }
                }
                CustomAdapterDettaglioDipendenti customAdapter = new CustomAdapterDettaglioDipendenti(SchedaDipendenti.this, listPassaggi);
                mylist.setAdapter(customAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
