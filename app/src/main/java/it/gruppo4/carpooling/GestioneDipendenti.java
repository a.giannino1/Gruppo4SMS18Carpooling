package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import Custom.CustomAdapterGestioneDipendenti;
import models.User;

public class GestioneDipendenti extends Fragment {

    private DatabaseReference mDatabase;

    final ArrayList<User> dipList = new ArrayList<User>();
    ListView mList;
    ArrayAdapter<User> adapter;
    ArrayList<String> userKey = new ArrayList<String>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.activity_gestione_dipendenti, container, false);

        getActivity().setTitle("Gestione Dipendenti");
        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda").child(LoggedUserSession.Instance().getAziendaID());
        mList = (ListView) mView.findViewById(R.id.listView1);
        adapter = new ArrayAdapter<User>(getActivity(), android.R.layout.simple_list_item_1, dipList);

        //Tutti i dipendenti aziendali
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dipendente : dataSnapshot.child("dipendenti").getChildren()){
                    User u = dipendente.getValue(User.class);
                    dipList.add(u);
                    userKey.add(dipendente.getKey().toString());
                    CustomAdapterGestioneDipendenti customAdapter = new CustomAdapterGestioneDipendenti(getActivity(), dipList);
                    mList.setAdapter(customAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //OnClick su un Item della lista
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long id) {

                final String selectedUserID = userKey.get(pos);

                AlertDialog.Builder mDialog = new AlertDialog.Builder(getActivity());
                mDialog.setTitle("Conferma Account");
                mDialog.setMessage("Dipendente:\n" + dipList.get(pos).getNome() + " " + dipList.get(pos).getCognome());
                mDialog.setCancelable(true);

                mDialog.setPositiveButton(
                        "Conferma",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mDatabase.child("dipendenti").child(selectedUserID).child("stato").setValue("Confermato");
                                dialog.cancel();
                                getActivity().finish();
                                startActivity(getActivity().getIntent());
                                Toast.makeText(getActivity(), R.string.confermato , Toast.LENGTH_SHORT).show();

                                Intent goManager = new Intent(getActivity(), HomeActivity.class);
                                startActivity(goManager);
                            }
                        });

                mDialog.setNegativeButton(
                        "Annulla",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = mDialog.create();
                alert11.show();
            }
        });

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}