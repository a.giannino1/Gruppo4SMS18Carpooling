package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

import Custom.turniCustom;
import models.Azienda;
import models.Turno;

public class GestioneTurni extends Fragment {
    EditText apertura, chiusura;
    private DatabaseReference mDatabase;
    String aziendaID = LoggedUserSession.Instance().getAziendaID();
    ListView mylist;
    FloatingActionButton floatingActionButton;
    ArrayList<String> sTurno = new ArrayList<String>();
    ArrayList<Turno> listTurni = new ArrayList<Turno>();
    Long sApertura, sChiusura;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.activity_gestione_turni, container, false); //riferimento al layout
        Objects.requireNonNull(getActivity()).setTitle("Gestione Turni");

        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");
        visualizzoTurni();

        mylist = (ListView) mView.findViewById(R.id.listaTurni);
        floatingActionButton = (FloatingActionButton) mView.findViewById(R.id.FAB);
        floatingActionButton.setOnClickListener(addTurno);

        //long press su un item della lista, elimina il turno selezionato
        mylist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> arg0, View arg1,
                                           final int pos, long id) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setMessage("Vuoi eliminare il turno selezionato?");

                dialogBuilder.setPositiveButton(
                        "Conferma",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sApertura = listTurni.get(pos).getApertura();
                                sChiusura = listTurni.get(pos).getChiusura();
                                mDatabase.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                                            if (azienda.getKey().equals(aziendaID)) {
                                                String a = azienda.getKey();
                                                for (DataSnapshot turno : azienda.child("turni").getChildren()) {
                                                    Turno u = turno.getValue(Turno.class);
                                                    if (sApertura.equals(u.getApertura()) && sChiusura.equals(u.getChiusura())) {
                                                        mDatabase.child(aziendaID).child("turni").child(turno.getKey()).removeValue();
                                                    }

                                                }
                                            }
                                        }

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                visualizzoTurni();
                                            }
                                        }, 100);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });

                dialogBuilder.setNegativeButton(
                        "Annulla",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


                AlertDialog alert = dialogBuilder.create();
                alert.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id

                alert.show();
                return true;
            }
        });

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //funzione per mostrare i turni
    private void visualizzoTurni() {
        listTurni.clear();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                    if (azienda.getKey().equals(aziendaID)) {
                        Azienda u = azienda.getValue(Azienda.class);
                        for (DataSnapshot turno : azienda.child("turni").getChildren()) {
                            Turno t = turno.getValue(Turno.class);
                            sTurno.add(turno.getKey());
                            listTurni.add(t);

                        }
                    }
                }

                turniCustom customAdapter = new turniCustom(getActivity(), listTurni, sTurno);
                mylist.setAdapter(customAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //aggiunge un turno quando si preme sul FAB
    View.OnClickListener addTurno = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.addturno, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setTitle("Aggiungo un turno lavorativo");

            apertura = (EditText) dialogView.findViewById(R.id.editApertura);
            chiusura = (EditText) dialogView.findViewById(R.id.editChiusura);
            dialogBuilder.setPositiveButton("Aggiungo Turno", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    long a, c;
                    a = new Long(apertura.getText().toString().trim());
                    c = new Long(chiusura.getText().toString().trim());

                    Turno turno = new Turno();
                    turno.setApertura(a);
                    turno.setChiusura(c);
                    mDatabase.child(aziendaID).child("turni").child("ConInizio" + a).setValue(turno);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent go = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                            startActivity(go);
                        }
                    }, 2000);
                }

            });


            AlertDialog b = dialogBuilder.create();
            b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
            b.show();

        }

    };


}