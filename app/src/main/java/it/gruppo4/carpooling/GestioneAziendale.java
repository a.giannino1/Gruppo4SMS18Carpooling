package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

import Custom.CustomAdapterDipendenti;
import models.User;

public class GestioneAziendale extends Fragment {
    private DatabaseReference mDatabase;
    String aziendaID = LoggedUserSession.Instance().getAziendaID();

    final ArrayList<User> listp = new ArrayList<User>();
    ListView mylist;
    ArrayAdapter<User> adapter;
    String s;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.activity_gestione_aziendale, container, false);
        Objects.requireNonNull(getActivity()).setTitle("Lista dipendenti");
        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");
        // recupero la lista dal layout
        mylist = (ListView) mView.findViewById(R.id.listView1);
        adapter = new ArrayAdapter<User>(getActivity(), android.R.layout.simple_list_item_1, listp);
        visualizzoDipendenti();

        //quando si clicca su un Item della lista
        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                Intent goManager = new Intent(getActivity(), SchedaDipendenti.class);
                goManager.putExtra("infoDipendente", listp.get(pos).getNome());
                startActivity(goManager);
            }
        });
        mylist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> arg0, View arg1,
                                           final int pos, long id) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setMessage("Vuoi eliminare il dipendente selezionato?");

                dialogBuilder.setPositiveButton(
                        "Conferma",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //ELIMINARE IL DIP SELEZIONATO DAL DIP.
                                s= listp.get(pos).getNome();
                                mDatabase.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                                            if (azienda.getKey().equals(aziendaID)) {
                                                String a = azienda.getKey();
                                                for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                                                    User u = dipendente.getValue(User.class);
                                                    if(s.equals(u.getNome())){
                                                        mDatabase.child(aziendaID).child("dipendenti").child(dipendente.getKey()).removeValue();
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });

                dialogBuilder.setNegativeButton(
                        "Annulla",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


                AlertDialog alert = dialogBuilder.create();
                alert.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id

                alert.show();
                return true;
            }
        });


        return mView;
        }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    private void visualizzoDipendenti() {
        //Leggere dal Database tutti i dipendenti
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                    if (azienda.getKey().equals(aziendaID)) {
                        String a = azienda.getKey();
                        for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                            User u = dipendente.getValue(User.class);
                            listp.add(u);
                            CustomAdapterDipendenti customAdapter = new CustomAdapterDipendenti(getContext(), listp);
                            mylist.setAdapter(customAdapter);

                        }
                    }
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    }
