package it.gruppo4.carpooling;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import models.Azienda;
import models.User;

public class RequiredMapDialogFragment extends DialogFragment implements OnMapReadyCallback {

    private String userID = LoggedUserSession.Instance().getUID();
    private String aziendaID = LoggedUserSession.Instance().getAziendaID();

    private String conducente;

    private Double lat, lon;
    private ArrayList<Double> latitudine = new ArrayList<>();
    private ArrayList<Double> longitudine = new ArrayList<>();

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");

    SupportMapFragment mapFragment;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_map_dialog, container, false);
        mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);

        conducente = getArguments() != null ? getArguments().getString("conducente") : null;

        getDati();

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        int lenght = latitudine.size();
        for(int i = 0; i < lenght; i++) {
            LatLng latLng = new LatLng(latitudine.get(i), longitudine.get(i));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            switch (i){
                case 0:
                    markerOptions.title(getResources().getString(R.string.lavoro));
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    break;
                case 1:
                    markerOptions.title(getResources().getString(R.string.casaAutista));
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    break;
                case 2:
                    markerOptions.title(getResources().getString(R.string.casaMia));
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    break;
            }
            googleMap.addMarker(markerOptions);
        }
    }

    private void getDati() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                    if (azienda.getKey().equals(aziendaID)){
                        Azienda a = azienda.getValue(Azienda.class);
                        converter(a.getVia());
                        latitudine.add(lat);
                        longitudine.add(lon);
                        for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()){
                            if(dipendente.getKey().equals(conducente)){
                                User u = dipendente.getValue(User.class);
                                converter(u.getVia());
                                latitudine.add(lat);
                                longitudine.add(lon);
                            }
                        }
                        for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()){
                            if(dipendente.getKey().equals(userID)){
                                User u = dipendente.getValue(User.class);
                                converter(u.getVia());
                                latitudine.add(lat);
                                longitudine.add(lon);
                            }
                        }
                    }
                }

                mapFragment.getMapAsync(RequiredMapDialogFragment.this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void converter(String string) {
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocationName(string, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            lat = addresses.get(0).getLatitude();
            lon = addresses.get(0).getLongitude();
        }
    }

    public void onDestroyView(){
        super.onDestroyView();
        Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }
}
