package it.gruppo4.carpooling;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_TIME = 3000;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.imgView);
        imageView.setImageResource(R.mipmap.ic_launcher_foreground);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the HomeActivity. */
                Intent home = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(home);
            }
        }, SPLASH_DISPLAY_TIME);
    }

}
