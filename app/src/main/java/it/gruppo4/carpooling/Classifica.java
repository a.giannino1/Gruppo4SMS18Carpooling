package it.gruppo4.carpooling;

import android.support.v4.app.Fragment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Custom.ClassificaCustom;
import models.Passeggero;
import models.User;



public class Classifica extends Fragment{
    String aziendaID = LoggedUserSession.Instance().getAziendaID();
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Azienda").child(aziendaID).child("dipendenti");

    final ArrayList<User> listaDipendenti = new ArrayList<User>();
    ListView mylist;
    Integer punti;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.activity_classifica, container, false);
        getActivity().setTitle("Classifica");

        mylist = (ListView) mView.findViewById(R.id.listClassifica);
        ricalcoloVisulizzoPunti();

        LayoutAnimationController controller
                = AnimationUtils.loadLayoutAnimation(
                getActivity(), R.anim.list_layout_controller);
        mylist.setLayoutAnimation(controller);


        return mView;
    }

    private void ricalcoloVisulizzoPunti() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dipSup : dataSnapshot.getChildren()) {
                    User uSup = dipSup.getValue(User.class);
                    punti = 0;
                    for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                        for (DataSnapshot passaggio : dipendente.child("passaggiOfferti").getChildren()){
                            for (DataSnapshot passeggero : passaggio.child("passeggeri").getChildren()){
                                Passeggero p = passeggero.getValue(Passeggero.class);
                                if (passeggero.getKey().equals(dipSup.getKey()))
                                    punti = punti + p.getPunti();
                            }
                        }
                    }
                    uSup.setPunti(punti);
                    mDatabase.child(dipSup.getKey()).setValue(uSup);
                }

                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        visualizzaPunti();
                    }
                }, 500);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void visualizzaPunti() {
        listaDipendenti.clear();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    User u = dipendente.getValue(User.class);
                    listaDipendenti.add(u);
                }

                Collections.sort(listaDipendenti, ordina);
                ClassificaCustom customAdapter = new ClassificaCustom(getContext(), listaDipendenti);
                mylist.setAdapter(customAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //ordinamento utilizzato
    Comparator<User> ordina = new Comparator<User>(){
        public int compare(User o1, User o2) {
            return o2.getPunti().compareTo(o1.getPunti());
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}