package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import Custom.CustomAdapterOffered;
import models.PassaggioOfferto;

public class OfferedPassagesFragment extends ListFragment {

    private String userID = LoggedUserSession.Instance().getUID();
    private String aziendaID = LoggedUserSession.Instance().getAziendaID();

    private DatabaseReference mRef;
    private ArrayList<PassaggioOfferto> arrayPassaggiOfferti = new ArrayList<>();
    private ArrayList<String> arrayIdPassaggiOfferti = new ArrayList<>();
    int i;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRef = FirebaseDatabase.getInstance().getReference().child("Azienda").
                child(aziendaID).child("dipendenti").child(userID);

        getPassaggiPasseggeriFromOfferti();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getPassaggiPasseggeriFromOfferti(){
        mRef.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.hasChild("passaggiOfferti")) {
                    Toast.makeText(getContext(), R.string.noPassageOffer, Toast.LENGTH_LONG).show();
                }else{
                    for (DataSnapshot passaggioOfferto : dataSnapshot.child("passaggiOfferti").getChildren()) {
                        PassaggioOfferto p = passaggioOfferto.getValue(PassaggioOfferto.class);
                        arrayPassaggiOfferti.add(p);
                        arrayIdPassaggiOfferti.add(passaggioOfferto.getKey());
                    }
                    if (getContext() != null){
                        CustomAdapterOffered customAdapter = new CustomAdapterOffered(getContext(), arrayPassaggiOfferti);
                        setListAdapter(customAdapter);
                        registerForContextMenu(getListView());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void onStart(){
        super.onStart();
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OfferedMapDialogFragment newFragment = new OfferedMapDialogFragment();
                Bundle b = new Bundle();

                if (!arrayIdPassaggiOfferti.isEmpty()){
                    b.putString("idPassaggio", arrayIdPassaggiOfferti.get(position));
                    newFragment.setArguments(b);
                    newFragment.show(getFragmentManager(), "mapDialog");
                }
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, v.getId(), 0, R.string.deleteOff);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = null;
        if(item.getMenuInfo() != null && item.getMenuInfo() instanceof AdapterView.AdapterContextMenuInfo){
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        }
        final int  position = info.position;

        if(item.getTitle().equals(getResources().getString(R.string.deleteOff))){

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.confirm);
            builder.setMessage(R.string.areyousure);

            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    deletePassage(position);
                }
            });

            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
        else {
            return false;
        }
        return true;
    }

    public void deletePassage(final int position){
        mRef.addValueEventListener(new ValueEventListener() {
            int i = -1;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot passaggioOfferto : dataSnapshot.child("passaggiOfferti").getChildren()) {
                    i++;
                    if (i == position){
                        mRef.child("passaggiOfferti").child(passaggioOfferto.getKey()).removeValue();
                        Toast.makeText(getActivity(), R.string.eliminato , Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
