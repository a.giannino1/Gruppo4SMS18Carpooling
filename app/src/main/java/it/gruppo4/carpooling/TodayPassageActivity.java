package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Custom.CustomAdapterTodayPassages;
import models.PassaggioOfferto;
import models.Passeggero;
import models.User;

public class TodayPassageActivity extends Fragment {

    private Date currentDateTime;
    private String currentDate;

    private DatabaseReference mDatabase;
    private DatabaseReference mRef;
    ArrayList<PassaggioOfferto> passOffList = new ArrayList<PassaggioOfferto>();
    ArrayList<User> autisti = new ArrayList<User>();
    ArrayList<String> autistiKey = new ArrayList<String>();
    ArrayList<String> passOffKey = new ArrayList<String>();
    ArrayList<User> passeggeri = new ArrayList<User>();
    ArrayList<String> passKey = new ArrayList<String>();

    String message = "Premere start per cominciare il tracking.";
    String positiveButton = "Start";
    String negativeButton = "Annulla";
    String statusTrack = "Fermo";

    ListView mList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.activity_today_passage, container, false);

        getActivity().setTitle("Passaggi di Oggi");

        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda").child(LoggedUserSession.Instance().getAziendaID());
        mList = (ListView) mView.findViewById(R.id.listView1);

        getTodayDate();
        getUserRequestedPassages();

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long id) {

                final String selectedAutistaKey = autistiKey.get(pos);
                final String selectedPassOffKey = passOffKey.get(pos);

                mRef = mDatabase.child("dipendenti").child(selectedAutistaKey).child("passaggiOfferti").child(selectedPassOffKey);

                mDatabase.child("dipendenti").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot dipendente : dataSnapshot.getChildren()) {

                            DataSnapshot selectedPass = dataSnapshot.child(selectedAutistaKey).child("passaggiOfferti").child(selectedPassOffKey);

                            PassaggioOfferto p = selectedPass.getValue(PassaggioOfferto.class);

                            //Se il tracking del passaggio selezionato è Fermo
                            if (p.getTracking().equals("Fermo")) {
                                //Se l'utente loggato è anche autista viene settato lo stato del tracking del passaggio a Partito
                                //Viene settato lo stato del passeggero autista loggato a Partito
                                if (selectedAutistaKey.equals(LoggedUserSession.Instance().getUID())) {
                                    statusTrack = p.getTracking();
                                }
                            } else {//Se il tracking del passaggio selezionato è Partito
                                //Se lo stato del tracking del passeggero è Fermo, allora viene settato a Partito
                                if (selectedPass.child("passeggeri").child(LoggedUserSession.Instance().getUID()).getValue(Passeggero.class).getTracking().equals("Fermo")) {
                                    statusTrack = "PasseggeroFermo";
                                } else {
                                    //Se lo stato del tracking del passeggero è Partito, allora vengono mostrati gli utenti tracciati
                                    StringBuilder tracked = new StringBuilder();
                                    tracked.append("Passeggeri che hanno cominciato il tracking:\n");
                                    for (DataSnapshot passeggero : dataSnapshot.child(selectedAutistaKey).child("passaggiOfferti").child(selectedPassOffKey).child("passeggeri").getChildren()) {
                                        if (dipendente.getKey().equals(passeggero.getKey())) {
                                            passeggeri.add(dipendente.getValue(User.class));
                                            passKey.add(dipendente.getKey());
                                            tracked.append("- " + dipendente.getValue(User.class).getNome() + " " + dipendente.getValue(User.class).getCognome() + "\n");
                                        }
                                    }
                                    message = tracked.toString();
                                    statusTrack = "PasseggeroFermo";
                                    positiveButton = "Ok";

                                    if (selectedAutistaKey.equals(LoggedUserSession.Instance().getUID())) {
                                        positiveButton = "Termina";
                                        statusTrack = "TerminaTracking";
                                    }


                                }
                            }
                        }
                        displayAlertDialog(message, positiveButton, negativeButton, statusTrack);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void displayAlertDialog(String message, String positiveButton, String negativeButton, final String statusTracking) {

        Context context = getActivity();
        String title = "Tracking";

        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        ad.setMessage(message);

        ad.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                switch (statusTracking) {
                    case "Fermo":
                        mRef.child("tracking").setValue("Partito");
                        mRef.child("passeggeri").child(LoggedUserSession.Instance().getUID()).child("tracking").setValue("Partito");
                        break;
                    case "PasseggeroFermo":
                        mRef.child("passeggeri").child(LoggedUserSession.Instance().getUID()).child("tracking").setValue("Partito");
                        break;
                    case "Partito":
                        break;
                    case "TerminaTracking":
                        mRef.child("tracking").setValue("Arrivato");
                        for (int i = 0; i < passKey.size(); i++) {
                            mRef.child("passeggeri").child(passKey.get(i)).child("tracking").setValue("Arrivato");
                        }
                        break;
                }
                dialog.cancel();
                getActivity().finish();
                startActivity(getActivity().getIntent());
            }
        });

        ad.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });
        ad.show();
    }

    private void getTodayDate() {
        currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = sdfData.format(currentDateTime.getTime());
    }

    private void getUserRequestedPassages() {
        //Tutti i dipendenti aziendali
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.child("dipendenti").getChildren()) {
                    for (DataSnapshot passOfferto : dipendente.child("passaggiOfferti").getChildren()) {
                        for (DataSnapshot passeggero : passOfferto.child("passeggeri").getChildren()) {
                            PassaggioOfferto po = passOfferto.getValue(PassaggioOfferto.class);
                            User user = dipendente.getValue(User.class);
                            if (LoggedUserSession.Instance().getUID().equals(passeggero.getKey()) && po.getData().equals(currentDate) && !po.getTracking().equals("Arrivato")) {
                                autisti.add(user);
                                autistiKey.add(dipendente.getKey());
                                passOffList.add(po);
                                passOffKey.add(passOfferto.getKey());
                                CustomAdapterTodayPassages customAdapter = new CustomAdapterTodayPassages(getActivity(), passOffList, autisti);
                                mList.setAdapter(customAdapter);
                            }

                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}