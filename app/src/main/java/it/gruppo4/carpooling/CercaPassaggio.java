package it.gruppo4.carpooling;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Azienda;
import models.PassaggioOfferto;
import models.Passeggero;
import models.User;

public class CercaPassaggio extends AppCompatActivity implements OnMapReadyCallback, DatePickerDialog.OnDateSetListener {

    EditText date;
    TextView nomeO, autoO, postiO;
    Button avvia;
    Spinner spinner2, spinner1, spinner3;
    private int mYear;
    private int mMonth;
    private int mDay;
    private double lat, lon;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    boolean flag = false;
    Date mDate;
    User us;
    Azienda az;

    private DatabaseReference mDatabase;

    ArrayList<String> listUser = new ArrayList<String>();
    ArrayList<String> listTurni = new ArrayList<String>();

    ArrayList<String> dipendenti = new ArrayList<String>();
    ArrayList<Double> latitudine = new ArrayList<Double>();
    ArrayList<Double> longitudine = new ArrayList<Double>();

    String aziendaID = LoggedUserSession.Instance().getAziendaID();
    String currendID = LoggedUserSession.Instance().getUID();

    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterTurni;

    ArrayList<String> arrayDipOffre = new ArrayList<String>();

    ArrayList<String> passaggiBuoni = new ArrayList<String>();
    ArrayList<User> arrayDipOffreUser = new ArrayList<>();
    ArrayList<PassaggioOfferto> passaggiBuoniPO = new ArrayList<>();

    private String userMarkerId;
    private User userMarker;

    Passeggero passeggero;
    private int j;
    private String pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cerca_passaggio);

        date = (EditText) findViewById(R.id.date);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner3 = (Spinner) findViewById(R.id.spinner3);
        avvia = (Button) findViewById(R.id.avviaMap);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listUser);
        adapterTurni = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listTurni);
        listUser.add("Tutti");

        //Accesso Database
        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda");

        date.setOnClickListener(selezionaData);
        avvia.setOnClickListener(Editable);

        getDipendentiAzienda();
        getTurni();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getView().setVisibility(View.GONE);

    }
    //Leggere dal Database tutti i turni
    private void getTurni() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                    if (azienda.getKey().equals(aziendaID)) {
                        Azienda u = azienda.getValue(Azienda.class);
                        for (DataSnapshot turno : azienda.child("turni").getChildren()) {
                            //come popolare uno spinner
                            listTurni.add(turno.getKey());
                            adapterTurni.notifyDataSetChanged();
                            spinner3.setAdapter(adapterTurni);
                            adapterTurni.setDropDownViewResource(android.R.layout.simple_spinner_item);

                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //Leggere dal Database tutti gli utenti
    private void getDipendentiAzienda() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                    if (azienda.getKey().equals(aziendaID)) {
                        for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                            User u = dipendente.getValue(User.class);
                            //come popolare uno spinner
                            listUser.add(u.getNome());
                            dipendenti.add(dipendente.getKey());
                            adapter.notifyDataSetChanged();
                            spinner2.setAdapter(adapter);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    View.OnClickListener selezionaData = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getFragmentManager(), "date picker");
            }
    };
    //algoritmo core dell'attività
    public View.OnClickListener Editable = new View.OnClickListener() {
        Map<String, PassaggioOfferto> p = new HashMap<>();

        @Override
        public void onClick(View v) {

            passaggiBuoni.clear();
            latitudine.clear();
            longitudine.clear();

            if (!flag) {
                mapFragment.getView().setVisibility(View.VISIBLE);
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot azienda : dataSnapshot.getChildren()) {

                                az = azienda.getValue(Azienda.class);
                                for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                                    if(!dipendente.getKey().equals(currendID)) {
                                        switch (spinner2.getSelectedItem().toString()) {
                                            case "Tutti":
                                                us = dipendente.getValue(User.class);
                                                p.putAll(us.getPassaggiOfferti());
                                                break;
                                            default:
                                                if(dipendente.getKey().equals(dipendenti.get(spinner2.getSelectedItemPosition() - 1))) {
                                                    us = dipendente.getValue(User.class);
                                                    p = us.getPassaggiOfferti();
                                                }
                                        }
                                    }
                                }
                        }


                        for (Map.Entry<String, PassaggioOfferto> passaggio : p.entrySet()) {
                            PassaggioOfferto po = passaggio.getValue();

                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                            String date = mDay + "/" + (mMonth + 1) + "/" + mYear;

                            try {
                                mDate = formatter.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if(po.getTratta().equals(spinner1.getSelectedItem().toString()) && po.getTurno().equals(spinner3.getSelectedItem().toString()) && po.getData().equals(formatter.format(mDate))){
                                if (!passaggiBuoni.contains(passaggio.getKey())){
                                    passaggiBuoni.add(passaggio.getKey());
                                    passaggiBuoniPO.add(po);
                                }
                            }
                        }

                        if(passaggiBuoni.isEmpty()){
                            Toast.makeText(CercaPassaggio.this,"NON CI SONO PASSAGGI", Toast.LENGTH_LONG).show();
                        }else{
                            mDatabase.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                                        if (azienda.getKey().equals(aziendaID)){
                                            for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                                                for (DataSnapshot passaggioOfferto : dipendente.child("passaggiOfferti").getChildren()) {
                                                    if(passaggiBuoni.contains(passaggioOfferto.getKey())){
                                                            User u = dipendente.getValue(User.class);
                                                            arrayDipOffreUser.add(u);
                                                            arrayDipOffre.add(dipendente.getKey());
                                                            converter(u.getVia());
                                                            latitudine.add(lat);
                                                            longitudine.add(lon);
                                                    }
                                                }
                                            }
                                        }
                                        mapFragment.getMapAsync(CercaPassaggio.this);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                flag = true;
            } else {
                mapFragment.getView().setVisibility(View.GONE);
                flag = false;

            }
        }
    };


    //Mostra tutti i marker e implementa il comportamento su ognugno di essi
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        int lenght = latitudine.size();
        for (int i = 0; i < lenght; i++) {
            LatLng latLng = new LatLng(latitudine.get(i), longitudine.get(i));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mMap.addMarker(markerOptions);

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                @Override
                public boolean onMarkerClick(Marker marker) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CercaPassaggio.this);
                    LayoutInflater inflater = CercaPassaggio.this.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.cerca_dialog, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setTitle("Richiedo il passaggio");

                    nomeO = (TextView) dialogView.findViewById(R.id.nomeO);
                    autoO = (TextView) dialogView.findViewById(R.id.autoO);
                    postiO = (TextView) dialogView.findViewById(R.id.postiO);

                    LatLng pos = marker.getPosition();

                    for (j = 0; j < passaggiBuoni.size(); j++){
                        if (latitudine.get(j).equals(pos.latitude)){

                            pass = passaggiBuoni.get(j);
                            userMarker = arrayDipOffreUser.get(j);
                            getIdFromUser(userMarker);
                            nomeO.setText(userMarker.getNome());
                            autoO.setText(passaggiBuoniPO.get(j).getAutomobile());
                            postiO.setText(passaggiBuoniPO.get(j).getPostiDisponibili() + "");


                            dialogBuilder.setPositiveButton("Richiedo Passaggio", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    mDatabase.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                            for (DataSnapshot azienda : dataSnapshot.getChildren()) {
                                                if(azienda.getKey().equals(aziendaID)){
                                                    for (DataSnapshot dipendente : azienda.child("dipendenti").getChildren()) {
                                                        for (DataSnapshot passaggioOfferto : dipendente.child("passaggiOfferti").getChildren()) {
                                                            for(DataSnapshot passegger: passaggioOfferto.child("passeggeri").getChildren()){
                                                            if(passaggioOfferto.getKey().equals(pass) && !passegger.getKey().equals(currendID)){
                                                                passeggero = new Passeggero();
                                                                passeggero.setTracking("Fermo");
                                                                passeggero.setStato_richiesta_passaggio("inSospeso");
                                                                passeggero.setPunti(5);
                                                                mDatabase.child(aziendaID).child("dipendenti").child(dipendente.getKey()).child("passaggiOfferti").
                                                                        child(pass).child("passeggeri").child(currendID).setValue(passeggero);
                                                                Intent goManager = new Intent(CercaPassaggio.this, MyPassages.class);
                                                                startActivity(goManager);
                                                               }else{
                                                                Toast.makeText(CercaPassaggio.this,"già chiesto", Toast.LENGTH_LONG).show();
                                                            }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                }
                            });
                        }
                    }

                    AlertDialog b = dialogBuilder.create();
                    b.show();

                    return false;
                }
            });

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        mYear = year;
        mMonth = month;
        mDay = dayOfMonth;
        date.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
    }

    //converte una string in latitudine e longitudine
    public void converter(String string) {

        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocationName(string, 5);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            lat = addresses.get(0).getLatitude();
            lon = addresses.get(0).getLongitude();

        }

    }


    private String getIdFromUser(final User user){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dipendente : dataSnapshot.getChildren()) {
                    User u = dipendente.getValue(User.class);

                    if(u.getCellulare() == (user.getCellulare())){
                        userMarkerId = dipendente.getKey();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        return userMarkerId;
    }
}

