package it.gruppo4.carpooling;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import models.Macchina;
import models.PassaggioOfferto;
import models.Passeggero;
import models.User;

public class OffriPassaggio extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    EditText date;
    Spinner trattaSpinner;
    Spinner autoSpinner;
    Spinner turnoSpinner;
    Button emptyBtn;
    Button offerBtn;

    private int mYear;
    private int mMonth;
    private int mDay;

    private long postiDisp;
    private int punti = 0;

    private DatabaseReference mDatabase;
    private String userID;
    private String aziendaID;

    //TERMINA
    Spinner occorrenzeSpn;
    String ripetizioneStr;
    int giorniRipetizione;

    ArrayList<String> carList = new ArrayList<String>();
    ArrayList<String> turnList = new ArrayList<String>();

    ArrayAdapter<String> carAdapter;
    ArrayAdapter<String> turnAdapter;
    SimpleDateFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offri_passaggio);

        date = (EditText) findViewById(R.id.date);
        trattaSpinner = (Spinner) findViewById(R.id.trattaSpinner);
        autoSpinner = (Spinner) findViewById(R.id.autoSpinner);
        turnoSpinner = (Spinner) findViewById(R.id.turnoSpinner);
        emptyBtn = (Button) findViewById(R.id.svuota);
        offerBtn = (Button) findViewById(R.id.aggiungiPassaggio);

        //TERMINA
        occorrenzeSpn = (Spinner) findViewById(R.id.occorrenzeSpn);

        emptyBtn.setOnClickListener(svuotaCampi);
        offerBtn.setOnClickListener(aggiungiPassaggio);
        date.setOnClickListener(selezionaData);

        carAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, carList);
        turnAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, turnList);

        userID = LoggedUserSession.Instance().getUID();
        aziendaID = LoggedUserSession.Instance().getAziendaID();

        //Accesso Database
        mDatabase = FirebaseDatabase.getInstance().getReference("Azienda").child(aziendaID);
        formatter = new SimpleDateFormat("dd/MM/yyyy");

        date.setText(getCurrentStingDate());
        getUserCar();
        getTurniAzienda();
    }

    //Questo metodo restituisce la data corrente sottoforma di String
    private String getCurrentStingDate() {
        String cDate = formatter.format(new Date());
        return formatDate(cDate);
    }

    //Questo metodo formatta la data dd/mm/yyyy
    private String formatDate(String date) {
        try {
            Date mDate = formatter.parse(date);
            Calendar c = Calendar.getInstance();
            c.setTime(mDate);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            mMonth = c.get(Calendar.MONTH);
            mYear = c.get(Calendar.YEAR);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    //Aggiunge i punti all'utente che offre un passaggio
    private void setPunti() {
        mDatabase.child("dipendenti").child(userID).child("passaggiOfferti").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot passOff : dataSnapshot.getChildren()) {
                    PassaggioOfferto m = passOff.getValue(PassaggioOfferto.class);
                    punti = punti + m.getPunti();
                }
                mDatabase.child("dipendenti").child(userID).child("punti").setValue(punti);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    View.OnClickListener selezionaData = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getFragmentManager(), "date picker");
        }
    };

    View.OnClickListener svuotaCampi = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getUserCar();
            getTurniAzienda();
            date.setText(getCurrentStingDate());
            Toast.makeText(getApplicationContext(), "Campi svuotati!", Toast.LENGTH_SHORT).show();
        }
    };

    View.OnClickListener aggiungiPassaggio = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            ripetizioneStr = occorrenzeSpn.getSelectedItem().toString();
            aggiungiPassaggio();
        }
    };

    //Core aggiungi passaggio
    private void aggiungiPassaggio() {
        PassaggioOfferto po;
        po = new PassaggioOfferto();
        Map<String, Passeggero> p = new HashMap<>();
        p.put(userID, new Passeggero("Confermato", "Fermo", 10));
        po.setTratta(trattaSpinner.getSelectedItem().toString());
        po.setAutomobile(autoSpinner.getSelectedItem().toString());
        po.setPostiDisponibili(postiDisp);
        po.setTracking("Fermo");
        po.setPasseggeri(p);
        po.setPunti(10);//punteggio per i passaggi offerti
        po.setTurno(turnoSpinner.getSelectedItem().toString());

        for (int i = 0; i < getGiorniRipetizione(); i++) {

            String date = mDay + "/" + (mMonth + 1) + "/" + mYear;
            String tDate = mYear + "_" + (mMonth + 1) + "_" + mDay;

            try {
                Date mDate = formatter.parse(date);
                po.setData(formatter.format(mDate));
                tDate = mYear + "_" + (mMonth + 1) + "_" + mDay;
                Calendar c = Calendar.getInstance();
                c.setTime(mDate);
                c.add(Calendar.DAY_OF_YEAR, 1);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mMonth = c.get(Calendar.MONTH);
                mYear = c.get(Calendar.YEAR);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            mDatabase.child("dipendenti").child(userID).child("passaggiOfferti").child("pass_ " + tDate + "_" + trattaSpinner.getSelectedItem().toString()).setValue(po);
        }
        Toast.makeText(getApplicationContext(), "Passaggio aggiunto!", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent home = new Intent(OffriPassaggio.this, HomeActivity.class);
                startActivity(home);
            }
        }, 2000);
        setPunti();
    }

    private int getGiorniRipetizione() {
        switch (ripetizioneStr) {
            case "1 giorno":
                giorniRipetizione = 1;
                break;
            case "2 giorni":
                giorniRipetizione = 2;
                break;
            case "3 giorni":
                giorniRipetizione = 3;
                break;
            case "4 giorni":
                giorniRipetizione = 4;
                break;
            case "5 giorni":
                giorniRipetizione = 5;
                break;
            case "6 giorni":
                giorniRipetizione = 6;
                break;
            case "1 settimana":
                giorniRipetizione = 7;
                break;
            case "2 settimane":
                giorniRipetizione = 14;
                break;
            case "3 settimane":
                giorniRipetizione = 21;
                break;
            case "1 mese":
                giorniRipetizione = 28;
                break;
        }
        return giorniRipetizione;

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        mYear = year;
        mMonth = month;
        mDay = day;
        date.setText(day + "/" + (month + 1) + "/" + year);
    }

    //Fornisce le auto dell'utente loggato
    private void getUserCar() {
        carList.clear();
        mDatabase.child("dipendenti").child(userID).child("macchine").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot macchina : dataSnapshot.getChildren()) {
                    Macchina m = macchina.getValue(Macchina.class);
                    postiDisp = m.getPosti() - 1;
                    carList.add(m.getModello());
                    carAdapter.notifyDataSetChanged();
                    autoSpinner.setAdapter(carAdapter);
                    carAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                }
                if (carList.size() == 0) {
                    Toast.makeText(getApplicationContext(), "Non puoi offrire un passaggio senza aggiungere prima la macchina!", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /* Create an Intent that will start the Menu-Activity. */
                            Intent home = new Intent(OffriPassaggio.this, HomeActivity.class);
                            startActivity(home);
                        }
                    }, 2000);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //FOrnisce i turni dell'azienda dell'utente loggato
    private void getTurniAzienda() {
        turnList.clear();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot turno : dataSnapshot.child("turni").getChildren()) {
                    turnList.add(turno.getKey());
                    turnAdapter.notifyDataSetChanged();
                    turnoSpinner.setAdapter(turnAdapter);
                    turnAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}