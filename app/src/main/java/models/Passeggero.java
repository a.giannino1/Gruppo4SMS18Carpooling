package models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Passeggero {

    private String stato_richiesta_passaggio;
    private String tracking;
    private int punti;

    public Passeggero (){}

    public Passeggero (String stato_richiesta_passaggio, String tracking, int punti){
        this.stato_richiesta_passaggio = stato_richiesta_passaggio;
        this.tracking = tracking;
        this.punti = punti;

    }

    public String getStato_richiesta_passaggio() {
        return stato_richiesta_passaggio;
    }

    public void setStato_richiesta_passaggio(String stato_richiesta_passaggio) {
        this.stato_richiesta_passaggio = stato_richiesta_passaggio;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public int getPunti() {
        return punti;
    }

    public void setPunti(int punti) {
        this.punti = punti;
    }
}

