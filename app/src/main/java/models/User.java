package models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class User {

    private long cellulare;
    private String citta;
    private String cognome;
    private Map<String, Macchina> macchine = new HashMap<String, Macchina>();
    private String mail;
    private String nome;
    private Map<String, PassaggioOfferto> passaggiOfferti = new HashMap<String, PassaggioOfferto>();
    private String password;
    private Integer punti;
    private String stato;
    private String via;

    public User (){

    }

    public User (long cellulare, String citta, String cognome, Map<String, Macchina> macchine, String mail, String nome, Map<String, PassaggioOfferto> passaggiOfferti,
                 String password, Integer punti,  String stato , String via){
        this.cellulare = cellulare;
        this.citta = citta;
        this.cognome = cognome;
        this.macchine = macchine;
        this.mail = mail;
        this.nome = nome;
        this.passaggiOfferti = passaggiOfferti;
        this.password = password;
        this.punti = punti;
        this.stato = stato;
        this.via = via;
    }

    public long getCellulare() {
        return cellulare;
    }

    public void setCellulare(long cellulare) {
        this.cellulare = cellulare;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public Map<String, Macchina> getMacchine() {
        return macchine;
    }

    public void setMacchine(Map<String, Macchina> macchine) {
        this.macchine = macchine;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Map<String, PassaggioOfferto> getPassaggiOfferti() {
        return passaggiOfferti;
    }

    public void setPassaggiOfferti(Map<String, PassaggioOfferto> passaggiOfferti) {
        this.passaggiOfferti = passaggiOfferti;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPunti() {
        return punti;
    }

    public void setPunti(Integer punti) {
        this.punti = punti;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

}
