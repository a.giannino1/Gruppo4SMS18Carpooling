package models;

import com.google.firebase.database.IgnoreExtraProperties;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Azienda {

    private String citta;
    private Map<String, User> dipendenti = new HashMap<String, User>();
    private String manager;
    private String nome;
    private Map<String, Turno> turni = new HashMap<String, Turno>();
    private String via;

    public Azienda (){}

    public Azienda (String citta, Map<String, User> dipendenti , String manager, String nome, Map<String, Turno> turni, String via){
        this.citta = citta;
        this.dipendenti = dipendenti;
        this.manager = manager;
        this.nome = nome;
        this.via = via;
        this.turni = turni;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public Map<String, User> getDipendenti() {
        return dipendenti;
    }

    public void setDipendenti(Map<String, User> dipendenti) {
        this.dipendenti = dipendenti;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Map<String, Turno> getTurni() {
        return turni;
    }

    public void setTurni(Map<String, Turno> turni) {
        this.turni = turni;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }
}
