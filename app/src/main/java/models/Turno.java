package models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Turno {

    private Long apertura;
    private Long chiusura;

    public Turno (){}

    public Turno (Long apertura, Long chiusura){
        this.apertura = apertura;
        this.chiusura = chiusura;
    }

    public Long getApertura() {
        return apertura;
    }

    public void setApertura(Long apertura) {
        this.apertura = apertura;
    }

    public Long getChiusura() {
        return chiusura;
    }

    public void setChiusura(Long chiusura) {
        this.chiusura = chiusura;
    }
}
