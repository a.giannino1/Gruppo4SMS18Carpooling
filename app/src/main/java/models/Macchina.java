package models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Macchina {

    private String modello;
    private long posti;

    public Macchina(){}

    public Macchina (String modello, long posti){
        this.modello = modello;
        this.posti = posti;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public long getPosti() {
        return posti;
    }

    public void setPosti(long posti) {
        this.posti = posti;
    }

}
