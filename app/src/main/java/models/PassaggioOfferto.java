package models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class PassaggioOfferto {

    private String automobile;
    private String data;
    private String turno;
    private Map<String, Passeggero> passeggeri = new HashMap<String, Passeggero>();
    private long postiDisponibili;
    private String tracking;
    private String tratta;
    private int punti;

    public PassaggioOfferto (){}

    public PassaggioOfferto (String automobile, String data, String turno, Map<String, Passeggero> passeggeri, long postiDisponibili, String tracking, String tratta, int punti){
        this.automobile = automobile;
        this.data = data;
        this.turno = turno;
        this.passeggeri = passeggeri;
        this.postiDisponibili = postiDisponibili;
        this.tracking = tracking;
        this.tratta = tratta;
        this.punti = punti;
    }

    public String getAutomobile() {
        return automobile;
    }

    public void setAutomobile(String automobile) {
        this.automobile = automobile;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public Map<String, Passeggero> getPasseggeri() {
        return passeggeri;
    }

    public void setPasseggeri(Map<String, Passeggero> passeggeri) {
        this.passeggeri = passeggeri;
    }

    public long getPostiDisponibili() {
        return postiDisponibili;
    }

    public void setPostiDisponibili(long postiDisponibili) {
        this.postiDisponibili = postiDisponibili;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public String getTratta() {
        return tratta;
    }

    public void setTratta(String tratta) {
        this.tratta = tratta;
    }

    public int getPunti() {
        return punti;
    }

    public void setPunti(int punti) {
        this.punti = punti;
    }
}
