package Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import it.gruppo4.carpooling.R;
import models.User;

public class ClassificaCustom extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<User> objects;
    String s = "";
    int i =1;
    private class ViewHolder {
        TextView pos;
        TextView nome;
        TextView punti;
    }
    public ClassificaCustom(Context context, ArrayList<User> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }
    public int getCount() {
        return objects.size();
    }

    public User getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ClassificaCustom.ViewHolder holder = null;
        if(convertView == null) {
            holder = new ClassificaCustom.ViewHolder();
            convertView = inflater.inflate(R.layout.custom_classifica, null);
            holder.nome = (TextView) convertView.findViewById(R.id.nome);
            holder.pos = (TextView) convertView.findViewById(R.id.posizione);
            holder.punti = (TextView) convertView.findViewById(R.id.punti);
            convertView.setTag(holder);
        } else {
            holder = (ClassificaCustom.ViewHolder) convertView.getTag();
        }
        holder.nome.setText(objects.get(position).getNome());
        holder.pos.setText(s + i);
        holder.punti.setText(s + objects.get(position).getPunti());

        i++;
        return convertView;
    }
}
