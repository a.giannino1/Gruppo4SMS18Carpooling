package Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import it.gruppo4.carpooling.R;
import models.PassaggioOfferto;
import models.Passeggero;

public class CustomAdapterDettaglioDipendenti extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<PassaggioOfferto> objects;

    private class ViewHolder {
        TextView turno;
        TextView data;
        TextView mezzo;
        TextView punti;
    }

    public CustomAdapterDettaglioDipendenti(Context context, ArrayList<PassaggioOfferto> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    public int getCount() {
        return objects.size();
    }

    public PassaggioOfferto getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CustomAdapterDettaglioDipendenti.ViewHolder holder;
        if(convertView == null) {
            holder = new CustomAdapterDettaglioDipendenti.ViewHolder();
            convertView = inflater.inflate(R.layout.dettaglio_dipendenti_custom_adapter, null);
            holder.turno = (TextView) convertView.findViewById(R.id.orario);
            holder.data = (TextView) convertView.findViewById(R.id.datario);
            holder.mezzo = (TextView) convertView.findViewById(R.id.mezzorio);
            holder.punti = (TextView) convertView.findViewById(R.id.punteggio);
            convertView.setTag(holder);
        } else {
            holder = (CustomAdapterDettaglioDipendenti.ViewHolder) convertView.getTag();
        }
        holder.turno.setText(objects.get(position).getTurno());
        holder.data.setText(objects.get(position).getData() + " ");
        holder.mezzo.setText(objects.get(position).getAutomobile());

        String nome = "";
        for (Map.Entry<String, Passeggero> entry : objects.get(position).getPasseggeri().entrySet())
        {
            if (entry.getValue().getPunti() == 10){
                nome = entry.getKey();
            }
        }

        holder.punti.setText(objects.get(position).getPasseggeri().get(nome).getPunti() + " ");
        return convertView;
    }
}
