package Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import it.gruppo4.carpooling.LoggedUserSession;
import it.gruppo4.carpooling.R;
import models.PassaggioOfferto;
import models.Passeggero;
import models.User;

public class CustomAdapterTodayPassages extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<PassaggioOfferto> passOfferti;
    private ArrayList<User> autisti;

    private class ViewHolder {
        TextView nomeAutista;
        TextView cellulareAutista;
        TextView statusPassaggio;
        TextView statoTracking;
        ImageView startTratta;
        ImageView stopTratta;
    }

    public CustomAdapterTodayPassages(Context context, ArrayList<PassaggioOfferto> passOfferti, ArrayList<User> autisti) {
        inflater = LayoutInflater.from(context);
        this.passOfferti = passOfferti;
        this.autisti = autisti;
    }

    public int getCount() {
        return passOfferti.size();
    }

    public PassaggioOfferto getItem(int position) {
        return passOfferti.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CustomAdapterTodayPassages.ViewHolder holder = null;
        if(convertView == null) {
            holder = new CustomAdapterTodayPassages.ViewHolder();
            convertView = inflater.inflate(R.layout.today_passages_custom_adapter, null);
            holder.nomeAutista = (TextView) convertView.findViewById(R.id.autista);
            holder.cellulareAutista = (TextView) convertView.findViewById(R.id.cellulare);
            holder.statusPassaggio = (TextView) convertView.findViewById(R.id.stato_passaggio);
            holder.startTratta = (ImageView) convertView.findViewById(R.id.start);
            holder.stopTratta = (ImageView) convertView.findViewById(R.id.stop);
            holder.statoTracking = (TextView)convertView.findViewById(R.id.stato_tracking);

            convertView.setTag(holder);
        } else {
            holder = (CustomAdapterTodayPassages.ViewHolder) convertView.getTag();
        }
        holder.nomeAutista.setText(autisti.get(position).getNome()+ " " + autisti.get(position).getCognome());
        holder.cellulareAutista.setText(autisti.get(position).getCellulare()+"");

        for (Map.Entry<String, Passeggero> passeggero : passOfferti.get(position).getPasseggeri().entrySet()) {
            Passeggero p = passeggero.getValue();
            if(passeggero.getKey().equals(LoggedUserSession.Instance().getUID())) {
                holder.statusPassaggio.setText(p.getStato_richiesta_passaggio());
                holder.statoTracking.setText(p.getTracking());
            }
        }

        switch(passOfferti.get(position).getTratta()){
            case "Casa-Lavoro":
                holder.startTratta.setImageResource(R.drawable.home);
                holder.stopTratta.setImageResource(R.drawable.work);
                break;
            case "Lavoro-Casa":
                holder.startTratta.setImageResource(R.drawable.work);
                holder.stopTratta.setImageResource(R.drawable.home);
                break;
            default:
                break;
        }
        return convertView;
    }
}
