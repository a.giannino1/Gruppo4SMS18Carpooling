package Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import it.gruppo4.carpooling.R;
import models.User;

public class CustomAdapterDipendenti extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<User> objects;

    private class ViewHolder {
        TextView nome;
    }
    public CustomAdapterDipendenti(Context context, ArrayList<User> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }
    public int getCount() {
        return objects.size();
    }

    public User getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomAdapterDipendenti.ViewHolder holder = null;
        if(convertView == null) {
            holder = new CustomAdapterDipendenti.ViewHolder();
            convertView = inflater.inflate(R.layout.dipendenti_custom_adapter, null);
            holder.nome = (TextView) convertView.findViewById(R.id.nome);
            convertView.setTag(holder);
        } else {
            holder = (CustomAdapterDipendenti.ViewHolder) convertView.getTag();
        }
        holder.nome.setText(objects.get(position).getNome());
        return convertView;
    }
}
