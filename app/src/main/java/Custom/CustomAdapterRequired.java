package Custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.gruppo4.carpooling.LoggedUserSession;
import it.gruppo4.carpooling.R;
import models.PassaggioOfferto;

public class CustomAdapterRequired extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<PassaggioOfferto> objects;

    private String userID = LoggedUserSession.Instance().getUID();

    public class ViewHolder {
        TextView turno;
        TextView dataOra;
        TextView stato;
        ImageView startTratta;
        ImageView stopTratta;
        ImageView arrowTratta;
    }

    public CustomAdapterRequired(Context context, ArrayList<PassaggioOfferto> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    public int getCount() {
        return objects.size();
    }

    public PassaggioOfferto getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomAdapterRequired.ViewHolder holder;
        if(convertView == null) {
            holder = new CustomAdapterRequired.ViewHolder();
            convertView = inflater.inflate(R.layout.required_adapter, null);
            holder.dataOra = convertView.findViewById(R.id.dataOra);
            holder.stato = convertView.findViewById(R.id.stato);
            holder.startTratta = convertView.findViewById(R.id.start);
            holder.stopTratta = convertView.findViewById(R.id.stop);
            holder.arrowTratta = convertView.findViewById(R.id.arrow);
            holder.turno = convertView.findViewById(R.id.turno);
            convertView.setTag(holder);
        } else {
            holder = (CustomAdapterRequired.ViewHolder) convertView.getTag();
        }
        switch (objects.get(position).getTratta()) {
            case "Casa-Lavoro":
                holder.startTratta.setImageResource(R.drawable.home);
                holder.stopTratta.setImageResource(R.drawable.work);
                break;
            case "Lavoro-Casa":
                holder.startTratta.setImageResource(R.drawable.work);
                holder.stopTratta.setImageResource(R.drawable.home);
                break;
            default:
                break;
        }

        holder.dataOra.setText(objects.get(position).getData());
        holder.stato.setText(objects.get(position).getPasseggeri().
                get(userID).getStato_richiesta_passaggio());

        holder.turno.setText(objects.get(position).getTurno());

        return convertView;
    }
}