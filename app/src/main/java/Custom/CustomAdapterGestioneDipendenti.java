package Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.gruppo4.carpooling.R;
import models.User;

public class CustomAdapterGestioneDipendenti extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<User> objects;

    private class ViewHolder {
        TextView name;
        TextView email;
        TextView statusAccount;
        ImageView statusImg;
    }

    public CustomAdapterGestioneDipendenti(Context context, ArrayList<User> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    public int getCount() {
        return objects.size();
    }

    public User getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomAdapterGestioneDipendenti.ViewHolder holder = null;
        if(convertView == null) {
            holder = new CustomAdapterGestioneDipendenti.ViewHolder();
            convertView = inflater.inflate(R.layout.gestione_dipendenti_custom_adapter, null);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.email = (TextView) convertView.findViewById(R.id.email);
            holder.statusAccount = (TextView) convertView.findViewById(R.id.status);
            holder.statusImg = (ImageView) convertView.findViewById(R.id.status_img);
            convertView.setTag(holder);
        } else {
            holder = (CustomAdapterGestioneDipendenti.ViewHolder) convertView.getTag();
        }
        holder.name.setText(objects.get(position).getNome()+ " " + objects.get(position).getCognome());
        holder.email.setText(objects.get(position).getMail());
        holder.statusAccount.setText(objects.get(position).getStato());
        switch(objects.get(position).getStato()){
            case "Confermato":
                holder.statusImg.setImageResource(R.drawable.green_status);
                break;

            case "In Sospeso":
                holder.statusImg.setImageResource(R.drawable.red_status);
                break;
            default:
                holder.statusImg.setImageResource(R.drawable.red_status);
                break;
        }

        return convertView;
    }
}
