package Custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import it.gruppo4.carpooling.R;
import models.Turno;

public class turniCustom extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Turno> turni;
   // private ArrayList<String> s;
    private class ViewHolder {
        TextView apertura;
        TextView chiusura;
      //  TextView turno;
    }
    public turniCustom(Context context, ArrayList<Turno> turni , ArrayList<String> s) {
        inflater = LayoutInflater.from(context);
        this.turni = turni;
        //this.s = s;

    }
    public int getCount() {
        return turni.size();
    }

    public Turno getItem(int position) {
        return turni.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        turniCustom.ViewHolder holder = null;
        if(convertView == null) {
            holder = new turniCustom.ViewHolder();
            convertView = inflater.inflate(R.layout.activity_turni_custom, null);
            holder.apertura = (TextView) convertView.findViewById(R.id.apertura);
            holder.chiusura = (TextView) convertView.findViewById(R.id.chiusura);
           // holder.turno =  (TextView) convertView.findViewById(R.id.turno);

            convertView.setTag(holder);
        } else {
            holder = (turniCustom.ViewHolder) convertView.getTag();
        }
        holder.apertura.setText(turni.get(position).getApertura() + "");
        holder.chiusura.setText(turni.get(position).getChiusura() + "");
        //holder.turno.setText(s.get(position).toString());

        return convertView;
    }
}
